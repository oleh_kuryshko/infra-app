variable "region" {}
variable "environment_name" {}
variable "environment_id" {}
variable "state_bucket" {}
variable "purpose" {}

variable "instance_type" {
  default = "t3.medium"
}
variable "volume_size" {
  default = 20
}
variable "device_name" {
  default = "/dev/sda1"
}
variable "launch_template_version" {
  default = "$Latest"
}
variable "min_size" {
  default = 1
}
variable "max_size" {
  default = 2
}
variable "desired_capacity" {
  default = 1
}
variable "subdomain" {}
# GitLab registry
variable "token_registry" {}
variable "user_registry" {}
variable "ci_registry" {}
variable "ci_project_group" {}
variable "ci_project_name_fr" {}
variable "ci_project_name_back" {}

